FROM --platform=linux/amd64 python:3.8.18
LABEL authors="innocentkithinji"

WORKDIR /app

COPY . /app
RUN pip install --upgrade pip

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "main.py" ]