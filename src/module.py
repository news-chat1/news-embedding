import json
import traceback
from datetime import datetime
from typing import List

from pymilvus import connections
import tiktoken

from core.embedding.embedding import get_embedding, correct_encode_string
from core.logging import setup_logging
from core.queues.Consumer import BaseConsumer
from .config import (milvus_collection, milvus_summary_collection, milvus_port, milvus_host, milvus_user,
                     milvus_password)
from .helpers.vector_db import create_article_collection


logger = setup_logging(__name__)
logger.info(f"Connecting to Milvus: {milvus_host}:{milvus_port}", extra={'user': milvus_user})

connections.connect("default", uri=f"https://{milvus_host}:{milvus_port}", user=milvus_user, password=milvus_password)



class VectorConsumer(BaseConsumer):

    def parse_message(self, data: List[str]) -> (bool, list):
        logger.info(f"Got Messages: {len(data)}")
        articles_collection = create_article_collection(milvus_collection)
        summaries_collection = create_article_collection(milvus_summary_collection)
        titles_collection = create_article_collection('titles')
        try:
            articles = [json.loads(article) for article in data]
            articles = [article for article in articles if correct_encode_string(article.get('text', ''))]
            if len(articles) == 0:
                logger.info(f'All texts in batch were too long', extra={'num_articles': len(data)})
                return True, []
            texts = [article.get('text', '') for article in articles]
            text_embeddings = get_embedding(texts)
            summaries = [article.get('summary', '') for article in articles]
            summary_embeddings = get_embedding(summaries)
            titles = [article.get('title', '') for article in articles]
            title_embeddings = get_embedding(titles)

            urls = [article.get('url', '') for article in articles]
            top_image = [article.get('top_image', '') for article in articles]
            publish_dates = [article.get('publish_date', '') for article in articles]
            parse_dates = [article.get('parse_date', '') for article in articles]

            for i in range(len(articles)):
                publish_date = publish_dates[i]
                parse_date = parse_dates[i]
                if publish_date == '':
                    publish_dates[i] = parse_date

            date_format = "%d/%m/%Y %H:%M:%S"

            publish_dates = [int(datetime.strptime(date, date_format).timestamp()) for date in publish_dates]
            parse_dates = [int(datetime.strptime(date, date_format).timestamp()) for date in parse_dates]

            texts = [text[:45000] for text in texts]
            summaries = [text[:45000] for text in summaries]
            titles = [text[:45000] for text in titles]

            logger.info('Inserting into Milvus')

            url_lengths = [len(url) for url in urls]
            top_image_lengths = [len(image) for image in top_image]

            for i, url in enumerate(urls):
                if url_lengths[i] > 45000:
                    print(f'URL too long: {url}')

            for i, image in enumerate(top_image):
                if top_image_lengths[i] > 45000:
                    print(f'Image too long: {image}')

            try:
                articles_collection.insert([
                    text_embeddings,
                    texts,
                    urls,
                    top_image,
                    publish_dates,
                    parse_dates
                ])

                summaries_collection.insert([
                    summary_embeddings,
                    summaries,
                    urls,
                    top_image,
                    publish_dates,
                    parse_dates
                ])

                titles_collection.insert([
                    title_embeddings,
                    titles,
                    urls,
                    top_image,
                    publish_dates,
                    parse_dates
                ])

                logger.info(f"Inserted {len(data)} articles into Milvus")
            except Exception as e:
                traceback.print_exc()
                logger.error(f'Unable to insert into Milvus', extra={'Error': e})
                return False, data
            return True, []
        except Exception as e:
            print(f'Error: {e}')
            return False, data
