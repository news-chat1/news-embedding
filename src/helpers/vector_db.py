from pymilvus import FieldSchema, DataType, CollectionSchema, Collection


def create_article_collection(collection_name='articles', description=''):
    try:
        collection = Collection(collection_name)
    except Exception:
        fields = [
            FieldSchema(name="id", dtype=DataType.INT64, is_primary=True, auto_id=True),
            FieldSchema(name='embedding', dtype=DataType.FLOAT_VECTOR, dim=1536),
            FieldSchema(name='text', dtype=DataType.VARCHAR, max_length=50000),
            FieldSchema(name='url', dtype=DataType.VARCHAR, max_length=50000),
            FieldSchema(name='top_image', dtype=DataType.VARCHAR, max_length=50000),
            FieldSchema(name='publish_date', dtype=DataType.INT64),
            FieldSchema(name='parse_date', dtype=DataType.INT64),
        ]

        schema = CollectionSchema(fields, description=description)

        collection = Collection(name=collection_name, schema=schema)

        collection.create_index(field_name='embedding',
                                index_params={"index_type": "HNSW",
                                              "metric_type": "L2",
                                              "params": {
                                                  "M": 16,
                                                  "efConstruction": 200,
                                              }})

    return collection
