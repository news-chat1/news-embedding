import os
openai_key = os.environ.get('OPENAI_KEY')

milvus_host = os.environ.get('MILVUS_HOST', 'localhost')
milvus_port = os.environ.get('MILVUS_PORT', '19530')
milvus_user = os.environ.get('MILVUS_USER', "")
milvus_password = os.environ.get('MILVUS_PASSWORD', "")
milvus_collection = os.environ.get('MILVUS_COLLECTION', 'articles')
milvus_summary_collection = os.environ.get('MILVUS_SUMMARY_COLLECTION', 'summaries')



subscription_topic = os.getenv('SUBSCRIPTION_TOPIC')
push_topic = os.getenv('PUSH_QUEUE')
rabbitmq_host = os.getenv('RABBITMQ_HOST', 'localhost')
rabbitmq_port = int(os.getenv('RABBITMQ_PORT', '5672'))
rabbitmq_user = os.getenv('RABBITMQ_USER')
rabbitmq_pass = os.getenv('RABBITMQ_PASS')
prefetch_count = int(os.getenv('PREFETCH_COUNT', '1'))
workers = int(os.getenv('WORKERS', '12'))



# SUBSCRIPTION_TOPIC=articles_to_vec;PUSH_QUEUE=articles_to_vec;RABBITMQ_HOST=localhost;RABBITMQ_PORT=5672;RABBITMQ_USER=myuser;RABBITMQ_PASS=mypassword;PREFETCH_COUNT=50;WORKERS=12