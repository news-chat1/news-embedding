import os
import traceback
from typing import List

import tiktoken
from openai import OpenAI

from core.logging.log import setup_logging

openai_key = os.getenv('OPENAI_KEY')
default_text_embedding_model = os.getenv('DEFAULT_TEXT_EMBEDDING_MODEL', 'text-embedding-ada-002')

if openai_key is None:
    raise Exception('OPENAI_KEY environment variable not set')

client = OpenAI(api_key=openai_key)

logger = setup_logging(__name__)

encoder = tiktoken.encoding_for_model(default_text_embedding_model)


def correct_encode_string(string):
    num_tokens = len(encoder.encode(string))
    if num_tokens > 8000:
        return False
    return True


def get_embedding(text: List[str]) -> List[List[float]]:
    """Get the embedding for a text.

    Args:
        text (str): The text to embed.

    Returns:
        List[float]: The embedding.
    """
    try:
        response = client.embeddings.create(input=text, model=default_text_embedding_model)
        embeddings = [e.embedding for e in response.data]
        return embeddings
    except Exception as e:
        traceback.print_exc()
        logger.error(f'Unable to get embedding', extra={'Error': e, 'text': text})
        return []
