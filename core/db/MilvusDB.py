from typing import Dict, Any, List

from pymilvus import connections, Collection
from pymilvus.exceptions import SchemaNotReadyException


class MilvusDB:
    def __init__(self, host, username, pwd, collection_name, collection_schema=None, port='19530'):
        self.host = host
        self.port = port
        self.username = username
        self.pwd = pwd
        self.collection_name = collection_name
        self.collection_schema = collection_schema
        self.collection = self.get_collection()

    def connect(self):
        connections.connect(host=self.host, port=self.port, user=self.username, password=self.pwd)

    def disconnect(self):
        connections.disconnect()

    def insert_one(self, data: List[Any]):
        self.collection.insert(data)

    def insert_many(self, data: List[Any]):
        self.insert_one(data)

    def create_index(self, field_name, index_type='HNSW', metric_type='IP', params=None):
        if params is None:
            params = {
                "metric_type": metric_type,
                "index_type": index_type,
                "params": {"M": 16, "efConstruction": 500}
            }
        try:
            self.collection.create_index(
                field_name,
                params
            )
            return True
        except Exception as e:
            return False

    def find(self, query: List[float], output_fields: List[str], anns_field='embedding', param=None, limit=20,
             expression=None) -> List[Dict[str, Any]]:
        if param is None:
            param = {
                "metric_type": "IP",
                "index_type": "HNSW",
                "params": {"M": 16, "efConstruction": 500}
            }
        self.collection.load()
        resp = self.collection.search(data=query,
                                      anns_field=anns_field,
                                      param=param,
                                      limit=limit,
                                      expr=expression,
                                      output_fields=output_fields
                                      )
        results = []
        for result in resp:
            data = {field: result.fields[field] for field in output_fields}
            data['id'] = result.id

            results.append(data)

        return results

    def get_collection(self):
        try:
            collection = Collection(name=self.collection_name)
        except SchemaNotReadyException:
            raise Exception(f'Collection with given name: {self.collection_name} has not been created.')
        return collection
