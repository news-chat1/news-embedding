from concurrent.futures import ThreadPoolExecutor

from core.logging import setup_logging
from . import RabbitMQQueue

logger = setup_logging(__name__)


class BaseConsumer:
    def __init__(self, user: str, password: str, subscribe_topic: str, push_topic: str = None, host: str = 'localhost',
                 port: int = 5672, prefetch_count: int = 1, workers: int = 1, batch_size: int = 50):
        self.subscription_topic: str = subscribe_topic
        self.push_topic: str = push_topic
        self.rabbit_mq_user: str = user
        self.rabbit_mq_password: str = password
        self.rabbit_mq_host: str = host
        self.rabbit_mq_port: int = port
        self.prefetch_count: int = prefetch_count
        self.workers: int = workers
        self.messages = []
        self.batch_size = batch_size

        self.queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                   password=self.rabbit_mq_password,
                                   topic=self.subscription_topic,
                                   host=self.rabbit_mq_host,
                                   port=self.rabbit_mq_port,
                                   prefetch_count=self.prefetch_count)

    def parse_message(self, data: list) -> (bool, list):
        print(f'Got message: {data}')
        return False, data

    def run(self):
        logger.info(f'Starting consumer with {self.workers} workers')

        def process_message(channel, method, properties, body):
            logger.debug(f'Processing message: {body}')
            message = body.decode('utf-8')
            channel.basic_ack(delivery_tag=method.delivery_tag)
            self.messages.append(message)
            if len(self.messages) >= self.batch_size:
                parsed, un_parsed_messages = self.parse_message(self.messages)
                if not parsed and un_parsed_messages is not None:
                    for message in un_parsed_messages:
                        channel.basic_publish(exchange='',
                                              routing_key=self.subscription_topic,
                                              body=message)
                logger.info(
                    f'Processed message: {len(self.messages)}',
                    extra={'parsed': parsed, 'accepted': len(self.messages) - len(un_parsed_messages),
                           'failed': len(un_parsed_messages)})
                self.messages = []

        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            for i in range(self.workers):
                logger.info(f'Starting worker {i}')
                queue = RabbitMQQueue(user=self.rabbit_mq_user,
                                      password=self.rabbit_mq_password,
                                      topic=self.subscription_topic,
                                      host=self.rabbit_mq_host,
                                      port=self.rabbit_mq_port,
                                      prefetch_count=self.prefetch_count)
                try:
                    queue.connect()
                    executor.submit(queue.subscribe, callback=process_message)
                except Exception as e:
                    logger.error(f'Failed to connect to queue: {e}, worker: {i}')
                    raise e

