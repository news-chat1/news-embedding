from src.config import *
from src.module import VectorConsumer

if __name__ == '__main__':
    verifier_consumer = VectorConsumer(
        user=rabbitmq_user, password=rabbitmq_pass, subscribe_topic=subscription_topic, push_topic=push_topic,
        host=rabbitmq_host, port=rabbitmq_port, prefetch_count=prefetch_count, workers=workers, batch_size=1)
    verifier_consumer.run()
